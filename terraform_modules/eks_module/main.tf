module "cluster" {
  source          = "./modules/cluster"
  cluster_name    = var.cluster_name
  subnets         = var.subnets
  public_access   = var.public_access
  private_access  = var.private_access
  tags            = var.tags
  cluster_version = var.cluster_version
}


module "node_groups" {
  source            = "./modules/node_groups"
  node_group       = var.node_group
  cluster_name      = module.cluster.eks_cluster_id
  create_node_group = var.create_node_group
  subnets = var.subnets
}