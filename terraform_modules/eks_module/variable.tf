variable "cluster_name" {
  description = "EKS cluster name"
  default     = "samarth-eks"
  type        = string
}

variable "subnets" {
  description = "A list of subnets for worker nodes"
  type        = list(string)
}

variable "cluster_version" {
  description = "Kubernetes cluster version in EKS"
  type        = string
}

variable "public_access" {
  description = "Kubernetes cluster version in EKS"
  type        = bool
}
variable "private_access" {
  description = "Kubernetes cluster version in EKS"
  type        = bool
}

variable "tags" {
  type = map(string)
}




variable "node_group" {
  description = "Paramters which are required for creating node group"
  type = map(object({
    instance_type          = list(string)
    disk_size              = number
    node_group_name        = string
    scale_desired_capacity = number
    scale_max_capacity     = number
    scale_min_capacity     = number
    security_group_ids = list(string)
    tags               = map(string)
    labels        = map(string)
    capacity_type = string
  }))
}




variable "create_node_group" {
  description = "Create node group or not"
  type        = bool
}