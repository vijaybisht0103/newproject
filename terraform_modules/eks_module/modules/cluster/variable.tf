variable "cluster_name" {
  description = "EKS cluster name"
  default     = "samarth-eks"
  type        = string
}

variable "subnets" {
  description = "A list of subnets for worker nodes"
  type        = list(string)
  }

variable "cluster_version" {
  description = "Kubernetes cluster version in EKS"
  type        = string
}

variable "public_access" {
    type        = bool
}
variable "private_access" {
  description = "Kubernetes cluster version in EKS"
  type        = bool
}

variable "tags" {
    type = map(string)
}