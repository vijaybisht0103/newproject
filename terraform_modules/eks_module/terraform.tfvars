name            = "samarth-eks"
#  subnets         = ["subnet-00bb0f3df4f8c901b", "subnet-0bb1d2c3321210d04"]
cluster_version = 1.23
public_access   = true
private_access  = false

tags = {
  Env = "dev"
}
create_node_group = true
node_group_name   = "test-eks-cluster"

node_group = {
  "group1" = {
    # subnets                =  ["subnet-00bb0f3df4f8c901b","subnet-0bb1d2c3321210d04"]
    security_group_ids     = ["sg-014e115dd4f719013"]
    instance_type          = ["t2.large"]
    scale_desired_capacity = 6
    node_group_name        = "eks-node-test"
    disk_size              = 100
    scale_max_capacity     = 15
    scale_min_capacity     = 2
    capacity_type          = "ON_DEMAND"
    tags = {
      env = "dev"
    }
    labels = { "node_group" : "worker1" }
  }
  
}