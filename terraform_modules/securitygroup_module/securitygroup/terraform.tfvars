inbound = [
  { port = 22,cidr_blocks = ["10.10.1.0/24"],protocol = "tcp",description = "allow ssh"},

  { port = 443,cidr_blocks = ["0.0.0.0/0"], protocol = "tcp",description = "allow https traffic"},
  
  { port = 80,cidr_blocks = ["0.0.0.0/0"], protocol = "tcp", description = "allow http traffic"}
    
]

outbound = [
  { port = 0,cidr_blocks = ["0.0.0.0/0"],protocol = "all",description = "allow outbound traffic"}
]

sg_tags = {
    "Name" = "samarth-app"
    }


sg_name = "samarth_sg"

description = "security group for samarth"

