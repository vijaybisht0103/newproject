variable "inbound" {
  description = "inbound rules for the security group"
  type = list(object({
    port        = number
    cidr_blocks = list(string)
    protocol = string
    description = string
  }))
}

variable "outbound" {
  description = "outbound rules for security group"
  type = list(object({
    port          = number
    cidr_blocks = list(string)
    protocol = string
    description = string

  }))
}

variable "sg_tags" {
  description = "a map of tags to add to security group"
  type = map
}

variable "sg_name" {
description = "a name for security group "
type = string
}

variable "description" {
  description = "description about security group"
  type = string
  }

 variable "vpc_id" {
  type = string
   }