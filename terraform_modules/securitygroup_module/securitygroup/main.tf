provider "aws" {
  region = "ap-south-1"

}

module "securitygroup" {
  source = "./modules/securitygroup"
  vpc_id = var.vpc_id
  inbound = var.inbound
  outbound = var.outbound
  tags = var.sg_tags
  name = var.sg_name
  description = var.description
}


