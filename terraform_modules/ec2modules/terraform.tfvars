instance_ami   = "ami-062df10d14676e201"
tags = {
  Name = "samarth_ec2"
}
instance_count = 2
instance_type  = ["t2.medium", "t2.medium", "t3.large"]
key_name       = "route53"
root_block_device = [
  {
     delete_on_termination = true 
     volume_size = 8 
     encrypted = true 
   volume_type = "gp2" 
  }
]
disable_api_termination = "false"
monitoring              = "true"
tenancy                 = "default"
security_group_ids  =  ["sg-0c1c8b5929e730010"]
associate_public_ip_address = true