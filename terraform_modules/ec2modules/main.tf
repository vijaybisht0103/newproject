

module "ec2_module" {
  source                  = "./modules/instance"
  instance_type           = var.instance_type
  instance_count          = var.instance_count  
  instance_ami            = var.instance_ami
  disable_api_termination = var.disable_api_termination
  monitoring              = var.monitoring
  tenancy                 = var.tenancy
  key_name                = var.key_name
  security_group_ids   = var.security_group_ids
  tags                    = var.tags
  root_block_device       = var.root_block_device
  associate_public_ip_address = var.associate_public_ip_address
}



