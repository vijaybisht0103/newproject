
variable "instance_ami" {
   type = string
   description = "AMI to use for the instance"
}

variable "tags" {
  type = map(string)
    description = " Map of tags to assign to the device"
}
 

variable "instance_count" {
  type = string
  description = "Instance number to use for the instance"
}


variable "instance_type" {
   type = list(string)
  description = "Instance type to use for the instance"
}

variable "key_name" {
  type = string
  description = "Name of the key that you have to define while create an key"
}


variable "root_block_device" {
  description = "Customize details about the root block device of the instance"
  
  type = list(object({
    delete_on_termination = bool
    volume_size = number
    encrypted   = bool
    volume_type = string
    
  }))
}


variable "disable_api_termination" {
  type = string
  }


variable "monitoring" {
  type = string
  description ="If true, the launched EC2 instance will have detailed monitoring enabled"
}

variable "tenancy" {
  type = string
  description = " Tenancy of the instance (if the instance is running in a VPC)." 
}


variable "security_group_ids" {
  type        = list(string)
  description = "public security group"
}

variable "associate_public_ip_address" {
  description = "whether to associate a public IP address with an instance in a VPC."
  type = bool
}