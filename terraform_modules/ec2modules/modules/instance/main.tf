data "aws_subnet" "subnet_id" {
    filter {
        name = "tag:Name"
        values = ["demo1"]
    }
}
resource "aws_instance"  "public_ec2"{
  count                       = length(var.instance_type)
  instance_type               = var.instance_type[count.index]
  subnet_id                   = data.aws_subnet.subnet_id.id
  ami                         = var.instance_ami
  disable_api_termination     = var.disable_api_termination
  monitoring                  = var.monitoring
  tenancy                     = var.tenancy
  key_name                    = var.key_name
  associate_public_ip_address = var.associate_public_ip_address
  vpc_security_group_ids      = var.security_group_ids
  tags                        = var.tags



  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
    delete_on_termination = root_block_device.value.delete_on_termination
    encrypted             = root_block_device.value.encrypted
    volume_size           = root_block_device.value.volume_size
    volume_type           = root_block_device.value.volume_type
    }
  }
 
}

