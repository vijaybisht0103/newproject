variable "vpc_cidr_block" {
  type        = string
  description = "CIDR for our VPC"
}

variable "vpc_tag" {
  type        = map(any)
  description = "tag for our VPC"
}

variable "enable_dns_support" {
  type        = bool
  description = "If this attribute is false, the Amazon-provided DNS server that resolves public DNS hostnames to IP addresses is not enabled."

}

variable "enable_dns_hostnames" {
  type        = bool
  description = "If this attribute is true, instances in the VPC get public DNS hostnames, but only if the enableDnsSupport attribute is also set to true."

}

variable "instance_tenancy" {
  type        = string
  description = "If this attribute is true, the provider ensures all EC2 instances that are launched in a VPC run on hardware that's dedicated to a single customer."

}

# variable "igw_validation" {
#   type        = bool
#   description = "will create igw if set to 1"
# }


variable "subnet" {
  description = "details for our subnet"
  type = map(object({
    cidr_block              = string
    map_public_ip_on_launch = bool
    availability_zone       = string
    tags                    = map(string)
    type                    = string
  }))
}
variable "igw_tags" {
  type        = map(any)
  description = "tags for our Internet Gateway"
}


variable "connectivity_type" {
  type        = string
  description = "Connectivity type for Gateway,valid value are public and private"
}

variable "nat_gateway_tag" {
  type        = map(any)
  description = "NAT Gateway tag"
}


# variable "create_nat_routes" {
#     type = bool
#   description = "create route for nat gateway"
# }
# variable "create_gateway_routes" {
#   type = bool
#   description = "create route for igw"
# }

# variable "destination_cidr_block" {
#   type = string  
# }

# variable "rt_tags" {
#   type = map(string)
# }


variable "public_route_name" {
  type        = map(string)
  description = "name of public route table"
}



variable "route_private_name" {
  type        = map(string)
  description = "name of private route table"

}

