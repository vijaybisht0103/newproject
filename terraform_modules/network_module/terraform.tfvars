vpc_cidr_block = "10.0.0.0/16"
vpc_tag = {
  Name        = "management_vpc"
  env = "dev"
}

enable_dns_support   = true
enable_dns_hostnames = false
instance_tenancy     = "default"

subnet = {
  "sb1" = {
    cidr_block              = "10.0.0.0/24"
    map_public_ip_on_launch = "true"
    availability_zone       = "ap-south-1a"
    type                    = "public"
    tags = {
      Name = "Public_Subnet"
    }
  }
  "sb2" = {
    cidr_block              = "10.0.1.0/24"
    map_public_ip_on_launch = "false"
    availability_zone       = "ap-south-1b"
    type                    = "private"
    tags = {
      Name = "Pvt_Subnet"
    }
  }
  "sb3" = {
    cidr_block              = "10.0.3.0/24"
    map_public_ip_on_launch = "false"
    availability_zone       = "ap-south-1b"
    type                    = "private"
    tags = {
      Name = "Pvt_Subnet"
    }
  }
  "sb4" = {
    cidr_block              = "10.0.5.0/24"
    map_public_ip_on_launch = "true"
    availability_zone       = "ap-south-1b"
    type                    = "public"
    tags = {
      Name = "Public_Subnet"
    }
  }
}

igw_tags = {
  name = "Internet-Gateway"
}

connectivity_type = "public"

nat_gateway_tag = {
  Name = "Nat-Gateway"
}


route_private_name = {
  Name = "Pvt_route"
}
public_route_name = {
  Name = "Pub_Route"
}
