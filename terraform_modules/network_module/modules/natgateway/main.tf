resource "aws_eip""elastic_ip"{
   vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.elastic_ip.id
  subnet_id     = var.nat_subnet_id
  connectivity_type = var.connectivity_type
  tags =  var.nat_gateway_tag
}