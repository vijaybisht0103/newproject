variable "nat_subnet_id" {
  type = string
  description = "IDs for our subnet"
}

variable "connectivity_type" {
  type =string
    description = "Connectivity type for Gateway,valid value are public and private"

}

variable "nat_gateway_tag" {
    type =map(any)
    description = "NAT Gateway tags"
  }

