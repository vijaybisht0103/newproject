

output "public_subnet_ids" {
  value = values(aws_subnet.public_subnet)[*].id

}

output "private_subnet_ids" {
  value = values(aws_subnet.pvt_subnet).*.id

}