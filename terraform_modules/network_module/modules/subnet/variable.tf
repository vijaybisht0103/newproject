

variable "vpc_id" {
    type =string
    description = "ID of VPC"
}

variable "subnet" {
  description = "details about the subnet"
  type = map(object({
    cidr_block = string
    map_public_ip_on_launch  = bool
    availability_zone = string
    tags   =   map(string)
    type = string
}))
}