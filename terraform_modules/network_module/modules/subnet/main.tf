resource "aws_subnet" "public_subnet"{
     for_each = {
    for k, v in var.subnet: k => v
    if "${v.type}" == "public"
  }
     vpc_id = var.vpc_id
    availability_zone = each.value.availability_zone
    cidr_block = each.value.cidr_block
    map_public_ip_on_launch = each.value.map_public_ip_on_launch
    tags  = each.value.tags
}

resource "aws_subnet" "pvt_subnet"{
    for_each = {
    for k, v in var.subnet: k => v
    if "${v.type}" == "private"
  }
      vpc_id = var.vpc_id
    availability_zone = each.value.availability_zone
    cidr_block = each.value.cidr_block
    map_public_ip_on_launch = each.value.map_public_ip_on_launch
    tags  = each.value.tags
}
