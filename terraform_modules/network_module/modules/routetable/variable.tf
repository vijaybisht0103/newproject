variable "vpc_id" {
    type = string
  }


# variable "destination_cidr_block" {
#   type = string  
# }

# variable "gateway_id" {
#   type = string
# }

# variable "nat_gateway_id" {
#   type = string
# }

# variable "create_nat_routes" {
#     type = bool
#    description = "create route for nat gateway"
 
# }
# variable "create_gateway_routes" {
#   type = bool
#   description = "create route for igw"

# }

# # variable "rt_subnet" {
# #   type = string
# # description = "subnet ids for association with route table"
# # }

# variable "rt_tags" {
#   type = map(string)
# }

variable "public_route_name" {
    type = map(string)
    description = "name of public route table"
}
variable "internet_gateway_id" {
  type = string
  description = "entry of internet gateway in public route table"
}
variable "publicsubnet_id" {
    type = list(string)
    description = "entry of public subnet in route table"
  
}
variable "route_private_name" {
    type = map(string)
    description = "name of private route table"
  
}
variable "privatesubnet_id" {
    type = list(string)
    description = "entry of private subnet in route table"
}
variable "nat_gateway_id" {
    type = string
    description = "entry of nat gateway in private route table"
  
}