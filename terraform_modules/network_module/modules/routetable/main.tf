resource "aws_route_table" "public_routetable" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.internet_gateway_id
  }
  tags = var.public_route_name
}

resource "aws_route_table_association" "public_route_table" {
   count = length(var.publicsubnet_id)
   subnet_id = var.publicsubnet_id[count.index]
   route_table_id = aws_route_table.public_routetable.id
 }

resource "aws_route_table" "private_routetable" {
  vpc_id = var.vpc_id
  tags =  var.route_private_name
  
}
resource "aws_route_table_association" "private_route_table" {
  count  = length(var.privatesubnet_id)
  subnet_id      = var.privatesubnet_id[count.index]
  route_table_id = aws_route_table.private_routetable.id
}
resource "aws_route" "nat" {
  route_table_id         = aws_route_table.private_routetable.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = var.nat_gateway_id
}