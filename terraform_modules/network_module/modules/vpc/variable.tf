variable "vpc_cidr_block" {
  type = string
  description = "CIDR for our VPC"
}

variable "vpc_tag" {
    type = map(any)
    description = "tag for our VPC"
}

variable "igw_tags" {
  type =map(any)
  description = "tag for our Internet Gateway"
  }


 variable "enable_dns_support" {
    type = bool
    description = "If this attribute is false, the Amazon-provided DNS server that resolves public DNS hostnames to IP addresses is not enabled."

}

variable "enable_dns_hostnames" {
    type = bool
    description = "If this attribute is true, instances in the VPC get public DNS hostnames, but only if the enableDnsSupport attribute is also set to true."

}

variable "instance_tenancy" {
    type = string
    description = "If this attribute is true, the provider ensures all EC2 instances that are launched in a VPC run on hardware that's dedicated to a single customer."

}
 
