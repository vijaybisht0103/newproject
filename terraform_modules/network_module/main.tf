
module "vpc_module" {
  source               = "./modules/vpc/"
  vpc_tag              = var.vpc_tag
  vpc_cidr_block       = var.vpc_cidr_block
  igw_tags             = var.igw_tags
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_hostnames
  instance_tenancy     = var.instance_tenancy
}

module "subnet_module" {
  source = "./modules/subnet"
  vpc_id = module.vpc_module.vpc_id
  subnet = var.subnet
}

module "natgateway_module" {
  source            = "./modules/natgateway"
  connectivity_type = var.connectivity_type
  nat_gateway_tag   = var.nat_gateway_tag
  nat_subnet_id     = module.subnet_module.public_subnet_ids[0]
}

module "routetable_module" {
  source              = "./modules/routetable"
  vpc_id              = module.vpc_module.vpc_id
  internet_gateway_id = module.vpc_module.internet_gateway_id
  nat_gateway_id      = module.natgateway_module.nat_gateway_id
  public_route_name   = var.public_route_name
  route_private_name  = var.route_private_name
  privatesubnet_id    = module.subnet_module.private_subnet_ids
  publicsubnet_id     = module.subnet_module.public_subnet_ids
}
