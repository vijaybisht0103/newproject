provider "aws" {
      region = "ap-south-1"
	  access_key = var.access_key
	  secret_key = var.secret_key

}


module "network" {
    source = "../../../../terraform_modules/network_module"
    vpc_tag = var.vpc_tag
    vpc_cidr_block = var.vpc_cidr_block
    igw_tags = var.igw_tags
    enable_dns_hostnames = var.enable_dns_hostnames
    enable_dns_support = var.enable_dns_hostnames
    instance_tenancy = var.instance_tenancy
    subnet = var.subnet
    connectivity_type = var.connectivity_type
    nat_gateway_tag =  var.nat_gateway_tag
    public_route_name = var.public_route_name
    route_private_name = var.route_private_name
 }

 module "securitygroup" {
        source = "../../../../terraform_modules/securitygroup_module/securitygroup"
        vpc_id = module.network.vpc_id
        inbound = var.inbound
        outbound = var.outbound
        sg_tags = var.sg_tags
        sg_name = var.sg_name
        description = var.description
}


 

# module "cluster" {
#      source = "../../../../terraform_modules/eks_module"
#     cluster_name    = var.cluster_name
#     subnets         = module.network.public_subnet_ids
#     public_access   = var.public_access
#     private_access  = var.private_access
#     tags            = var.tags
#     cluster_version = var.cluster_version
#     node_group       = var.node_group
#     create_node_group = var.create_node_group
# }
