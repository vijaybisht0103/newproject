variable "access_key" {
type =string
}


variable "secret_key" {
type = string
}

variable "vpc_cidr_block" {
  type = string
  description = "CIDR for our VPC"
}

variable "vpc_tag" {
  type = map(any)
  description = "tag for our VPC"
}

 variable "enable_dns_support" {
    type = bool
    description = "If this attribute is false, the Amazon-provided DNS server that resolves public DNS hostnames to IP addresses is not enabled."

}

variable "enable_dns_hostnames" {
    type = bool
    description = "If this attribute is true, instances in the VPC get public DNS hostnames, but only if the enableDnsSupport attribute is also set to true."

}

variable "instance_tenancy" {
    type = string
    description = "If this attribute is true, the provider ensures all EC2 instances that are launched in a VPC run on hardware that's dedicated to a single customer."

}

#  variable "igw_validation" {
#     type = bool
#     description = "will create igw if set to 1"
#     default = true
#       }


 variable "igw_tags" {
  type =map(any)
  description = "tags for our Internet Gateway"
  }



# variable "vpc_id" {
#     type =string
#     description = "ID of VPC"
# }

variable "subnet" {
  description = "details about the subnet"
  type = map(object({
    cidr_block = string
    map_public_ip_on_launch  = bool
    availability_zone = string
    tags   =   map(string)
    type = string
}))
}

variable "public_route_name" {
    type = map(string)
    description = "name of public route table"
}
# variable "internet_gateway_id" {
#   type = string
#   description = "entry of internet gateway in public route table"
# }
# variable "publicsubnet_id" {
#     type = list(string)
#     description = "entry of public subnet in route table"
  
# }
variable "route_private_name" {
    type = map(string)
    description = "name of private route table"
  
}
# variable "privatesubnet_id" {
#     type = list(string)
#     description = "entry of private subnet in route table"
# }
# variable "nat_gateway_id" {
#     type = string
#     description = "entry of nat gateway in private route table"
  
# # }
# variable "nat_subnet_id" {
#   type = string
#   description = "IDs for our subnet"
# }

variable "connectivity_type" {
  type =string
    description = "Connectivity type for Gateway,valid value are public and private"

}

variable "nat_gateway_tag" {
    type =map(any)
    description = "NAT Gateway tags"
    default = {}
}

# security group variable

variable "inbound" {
  description = "inbound rules for the security group"
  type = list(object({
    port        = number
    cidr_blocks = list(string)
    protocol = string
    description = string
  }))
}

variable "outbound" {
  description = "outbound rules for security group"
  type = list(object({
    port          = number
    cidr_blocks = list(string)
    protocol = string
    description = string

  }))
}

variable "sg_tags" {
  description = "a map of tags to add to security group"
  type = map
}

variable "sg_name" {
description = "a name for security group "
type = string
}

variable "description" {
  description = "description about security group"
  type = string
  }










# # eks cluster variable 

# variable "create_node_group" {
#   type = bool
# }
# variable "cluster_name" {
#   description = "EKS cluster name"
#   default     = "samarth-eks"
#   type        = string
# }



# variable "node_group" {
#   description = "Paramters which are required for creating node group"
#   type = map(object({
#     # subnets                = list(string)
#     instance_type          = list(string)
#     disk_size              = number
#     node_group_name        = string
#     scale_desired_capacity = number
#     scale_max_capacity     = number
#     scale_min_capacity     = number
#     security_group_ids = list(string)
#     tags               = map(string)
#     labels        = map(string)
#     capacity_type = string
#   }))
# }


# variable "cluster_version" {
#   description = "Kubernetes cluster version in EKS"
#   type        = string
# }

# variable "public_access" {
#     type        = bool
# }
# variable "private_access" {
#   description = "Kubernetes cluster version in EKS"
#   type        = bool
# }

# variable "tags" {
#     type        = map
# }

