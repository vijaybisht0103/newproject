access_key = "AKIATOTF6NV7VPMGBJOD"
secret_key = "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"



vpc_cidr_block = "10.0.0.0/16"
vpc_tag = {
  Name        = "Vpc_1"
  environment = "test"
}

enable_dns_support = true
enable_dns_hostnames = false
instance_tenancy     = "default"
subnet = {
  "sb1" = { 
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "ap-south-1a"
  type = "public"
  tags = {
  Name = "Public_Subnet"
  }
}
"sb2" = {
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "ap-south-1b"
    type = "private"
    tags = {
    Name = "Pvt_Subnet"
}
}
"sb3" = {
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "ap-south-1b"
    type = "private"
    tags = {
    Name = "Pvt_Subnet"
}
}
 "sb4" = {
    cidr_block = "10.0.5.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "ap-south-1b"
    type = "public"
    tags = {
    Name = "Public_Subnet"
}
} 
}
igw_tags = {
  name = "Internet-Gateway"
}

connectivity_type = "public"

nat_gateway_tag = {
  Name = "Nat-Gateway"
}


route_private_name = {
  Name = "Pvt_route"
}
public_route_name = {
  Name  = "Pub_Route"
}


# # eks cluster variable values
# name            = "samarth-eks"
# # subnets         = ["subnet-00bb0f3df4f8c901b", "subnet-0bb1d2c3321210d04"]
# cluster_version = 1.23
# public_access   = true
# private_access  = false

# tags = {
#   Env = "dev"
# }
# create_node_group = true
# node_group_name   = "test-eks-cluster"

# node_group = {
#   "group1" = {
#      security_group_ids     = ["sg-014e115dd4f719013"]
#     instance_type          = ["t2.large"]
#     scale_desired_capacity = 6
#     node_group_name        = "eks-node-test"
#     disk_size              = 100
#     scale_max_capacity     = 15
#     scale_min_capacity     = 2
#     capacity_type          = "ON_DEMAND"
#     tags = {
#       env = "dev"
#     }
#     labels = { "node_group" : "worker1" }
#   }
  
# }

# security group

inbound = [
  { port = 22,cidr_blocks = ["10.10.1.0/24"],protocol = "tcp",description = "allow ssh"},

  { port = 443,cidr_blocks = ["0.0.0.0/0"], protocol = "tcp",description = "allow https traffic"},
  
  { port = 80,cidr_blocks = ["0.0.0.0/0"], protocol = "tcp", description = "allow http traffic"}
    
]

outbound = [
  { port = 0,cidr_blocks = ["0.0.0.0/0"],protocol = "all",description = "allow outbound traffic"}
]

sg_tags = {
    "Name" = "samarth-app"
    }

sg_name = "samarth_sg"

description = "security group for samarth"
